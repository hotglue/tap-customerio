"""Customerio tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_customerio.streams import CustomerIdsStream, CustomersStream, SegmentsStream

STREAM_TYPES = [CustomerIdsStream, CustomersStream, SegmentsStream]


class TapCustomerio(Tap):
    """Customerio tap class."""

    name = "tap-customerio"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "api_key",
            th.StringType,
            required=True,
            description="The token to authenticate against the API service",
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        ),
        th.Property(
            "api_url",
            th.StringType,
            default="https://api.customer.io/v1",
            description="The url for the API service",
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapCustomerio.cli()
