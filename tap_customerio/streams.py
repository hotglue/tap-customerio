"""Stream type classes for tap-customerio."""

from typing import Any, Dict, Optional

import requests
from singer_sdk import typing as th

from tap_customerio.client import CustomerioStream


class CustomerIdsStream(CustomerioStream):
    """Define custom stream."""

    name = "customers_ids"
    path = "/customers"
    primary_keys = ["id"]
    replication_key = None
    rest_method = "POST"
    schema = th.PropertiesList(
        th.Property(
            "identifiers",
            th.ArrayType(
                th.ObjectType(
                    th.Property("cio_id", th.StringType),
                    th.Property("id", th.StringType),
                    th.Property("email", th.StringType),
                )
            ),
        ),
        th.Property("ids", th.ArrayType(th.StringType)),
    ).to_dict()

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        body = {"filter": {"and": [{"segment": {"id": 1}}]}}
        return body

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "ids": record["ids"],
        }

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        response_json = response.json()

        if response_json["next"] != "":
            next_page_token = response_json["next"]
        else:
            next_page_token = None
        return next_page_token

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        params["limit"] = 100
        if next_page_token:
            params["start"] = next_page_token
        return params


class CustomersStream(CustomerioStream):
    """Define custom stream."""

    name = "customers"
    path = "/customers/attributes"
    primary_keys = ["id"]
    replication_key = None
    records_jsonpath = "$.customers[*]"
    rest_method = "POST"
    parent_stream_type = CustomerIdsStream
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property(
            "identifiers",
            th.ObjectType(
                th.Property("cio_id", th.StringType),
                th.Property("id", th.StringType),
            ),
        ),
        th.Property(
            "attributes",
            th.ObjectType(
                th.Property("cio_id", th.StringType),
                th.Property("created_at", th.StringType),
                th.Property("id", th.StringType),
            ),
        ),
        th.Property(
            "timestamps",
            th.ObjectType(
                th.Property("cio_id", th.StringType),
                th.Property("created_at", th.StringType),
                th.Property("id", th.StringType),
            ),
        ),
        th.Property("unsubscribed", th.BooleanType),
        th.Property("devices", th.ArrayType(th.StringType)),
    ).to_dict()

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        body = {"ids": context["ids"]}
        return body


class SegmentsStream(CustomerioStream):
    """Define custom stream."""

    name = "segments"
    path = "/segments"
    primary_keys = ["id"]
    replication_key = None
    records_jsonpath = "$.segments[*]"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("deduplicate_id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("description", th.StringType),
        th.Property("state", th.StringType),
        th.Property("progress", th.StringType),
        th.Property("type", th.StringType),
        th.Property("tags", th.ArrayType(th.StringType)),
    ).to_dict()
